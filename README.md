# Proyecto de Gestión de Proyecto de Software

Este proyecto se desarrolla en colaboración como parte del curso de **Gestión de Proyectos** en la **Universidad del Bío-Bío**. Utilizaremos métodos iterativos y técnicas ágiles para construir una aplicación de backend y una app móvil para Android.

## Componentes del Proyecto

### 1. Backend (MERN Stack)

### 2. Aplicación para Android (React Native)

