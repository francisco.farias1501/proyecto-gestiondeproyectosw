import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { login, logout } from '../services/auth.service';

const LoginForm = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async () => {
    if (!email || !password) {
      setError('Por favor, completa todos los campos.');
      return;
    }

    try {
      await logout();
      await login({ email, password });
      const user = await AsyncStorage.getItem('user');
      if (user) {
        navigation.navigate('Publicaciones');
      } else {
        setError('Credenciales incorrectas.'); // Se establece el mensaje de error cuando las credenciales son incorrectas
      }
    } catch (error) {
      setError('Credenciales incorrectas.'); // Se establece el mensaje de error cuando hay un error en la autenticación
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Email</Text>
      <TextInput
        style={styles.input}
        onChangeText={setEmail}
        value={email}
        placeholder="Introduce tu email"
        keyboardType="email-address"
        accessibilityLabel="Email"
      />
      <Text style={styles.label}>Contraseña</Text>
      <TextInput
        style={styles.input}
        onChangeText={setPassword}
        value={password}
        placeholder="Introduce tu contraseña"
        secureTextEntry
        accessibilityLabel="Contraseña"
      />
      {error ? <Text style={styles.error}>{error}</Text> : null}
      <Button title="Enviar" onPress={handleSubmit} accessibilityLabel="Enviar" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  label: {
    alignSelf: 'flex-start',
    marginBottom: 5,
  },
  input: {
    width: '100%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
  errorMessage: {
    color: 'red',
    marginTop: 10, // Separación del mensaje de error del botón de enviar
  },
});

export default LoginForm;
