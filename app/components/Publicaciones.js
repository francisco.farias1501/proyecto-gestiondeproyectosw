import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { getPublicacion } from '../services/publicacion.service';

const Publicaciones = () => {
  const [jsonData, setJsonData] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getPublicacion();
        setJsonData(response);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <View>
      {jsonData && jsonData.data.map(item => (
        <View key={item._id}>
          <Text>Nombre: {item.nombre}</Text>
          <Text>Detalle: {item.detalle}</Text>
          <Text>Fecha: {item.fecha}</Text>
          {/* Agrega aquí más campos si es necesario */}
        </View>
      ))}
    </View>
  );
};

export default Publicaciones;
