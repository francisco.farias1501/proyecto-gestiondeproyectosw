import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { StyleSheet, Text, View } from 'react-native';
import LoginForm from './components/LoginForm'; // Asumiendo que esta es tu pantalla de inicio de sesión
import Home from './components/Home';
import Publicaciones from './components/Publicaciones';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={LoginForm} options={{ title: 'Iniciar Sesión' }} />

        <Stack.Screen name="Home" component={Home} options={{ title: 'Inicio' }} />

        <Stack.Screen name="Publicaciones" component={Publicaciones} options={{ title: 'Publicaciones' }} />

        {/* Puedes agregar más pantallas aquí */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
