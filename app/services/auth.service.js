import axios from './root.service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import cookies from 'js-cookie';
import { jwtDecode, JwtPayload } from 'jwt-decode';

export const login = async ({ email, password }) => {
  try {
    const response = await axios.post('auth/login', {
      email,
      password,
    });
    const { status, data } = response;

    if (status === 200) {
      const { email, roles } = await jwtDecode(data.data.accessToken);

      await AsyncStorage.setItem('user', JSON.stringify({ email, roles }));
      axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${data.data.accessToken}`;
      cookies.set('jwt-auth', data.data.accessToken, { path: '/' });
    }
  } catch (error) {
    console.log("auth.services -> login");
    console.log(error);
  }
};


export const logout = () => {
  AsyncStorage.removeItem('user');
  delete axios.defaults.headers.common['Authorization'];
  cookies.remove('jwt');
};

export const test = async () => {
  try {
    const response = await axios.get('/users');
    const { status, data } = response;
    if (status === 200) {
      console.log(data.data);
    }
  } catch (error) {
    console.log(error);
  }
};
