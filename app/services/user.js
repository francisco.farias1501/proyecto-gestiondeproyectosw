import axios from './root.service';

const API_URL = 'http://192.168.100.11:8080/api';

export const getUsers = async () => {
    const userId = localStorage.getItem('user');

    if (!userId) {
      throw new Error('User data not found in localStorage');
    }

    const userObject = JSON.parse(userId);
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      throw new Error('Access token not found in user roles');
    }


  try {
    const ruta = `${API_URL}/users/`;
    const response = await axios.get(ruta);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error fetching users data: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error fetching users: ${error.message}`);
  }
};

export const createUser = async (user) => {
    const userId = localStorage.getItem('user');

    if (!userId) {
      throw new Error('User data not found in localStorage');
    }

    const userObject = JSON.parse(userId);
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      throw new Error('Access token not found in user roles');
    }


  try {
    const ruta = `${API_URL}/users/`;
    const response = await axios.post(ruta, user);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error creating user: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error creating user: ${error.message}`);
  }
};

export const getUserById = async (id) => {
    const userId = localStorage.getItem('user');

    if (!userId) {
      throw new Error('User data not found in localStorage');
    }

    const userObject = JSON.parse(userId);
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      throw new Error('Access token not found in user roles');
    }


  try {
    const ruta = `${API_URL}/users/${id}`;
    const response = await axios.get(ruta);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error fetching user data: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error fetching user: ${error.message}`);
  }
};

export const updateUser = async (id, user) => {

  /*
  const json = {
    "username": user.username,
    "email": user.email,
    "newPassword": user.newPassword,
    "password": user.password,
    "roles": ["user"]
  };
*/
  try {
    const ruta = `${API_URL}/users/${id}`;
    const response = await axios.put(ruta, user);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error updating user: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error updating user: ${error.message}`);
  }
};

export const deleteUser = async (id) => {
    const userId = localStorage.getItem('user');

    if (!userId) {
      throw new Error('User data not found in localStorage');
    }

    const userObject = JSON.parse(userId);
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      throw new Error('Access token not found in user roles');
    }


  try {
    const ruta = `${API_URL}/users/${id}`;
    const response = await axios.delete(ruta);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error deleting user: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error deleting user: ${error.message}`);
  }
};
