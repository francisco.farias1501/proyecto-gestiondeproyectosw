import axios from './root.service';
import AsyncStorage from '@react-native-async-storage/async-storage';

const API_URL = 'http://192.168.100.11:8080/api';

export const getPublicacion = async () => {
  try {
    const userId = await AsyncStorage.getItem('user');

    if (!userId) {
      throw new Error('User data not found in AsyncStorage');
    }

    const userObject = JSON.parse(userId);
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;
  

    if (!accessToken) {
      throw new Error('Access token not found in user roles');
    }

    const ruta = `${API_URL}/publicacion/`;
    const response = await axios.get(ruta);

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else {
      throw new Error(`Error fetching users data: ${response.statusText}`);
    }
  } catch (error) {
    throw new Error(`Error fetching users: ${error.message}`);
  }
};
