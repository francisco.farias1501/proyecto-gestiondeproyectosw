// configFTP.js
import ftpClient from 'ftp';
import { FTP_URL, FTP_USER, FTP_PASSWORD } from './configEnv.js';
import { handleError } from '../utils/errorHandler.js';

/**
 * Establece la conexión con el servidor FTP.
 * @async
 * @function setupFTP
 * @throws {Error} Si no se puede conectar al servidor FTP.
 * @returns {Promise<ftpClient>} Una promesa que se resuelve con el cliente FTP conectado.
 */
async function setupFTP() {
  return new Promise((resolve, reject) => {
    const client = new ftpClient();
    client.on('ready', () => {
      console.log('=> Conectado al servidor FTP');
      resolve(client);
    });
    client.on('error', (err) => {
      handleError(err, '/configFTP.js -> setupFTP');
      reject(err);
    });
    client.connect({
      host: FTP_URL,
      user: FTP_USER,
      password: FTP_PASSWORD
    });
  });
}

export { setupFTP };
