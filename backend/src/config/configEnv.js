"use strict";
import { fileURLToPath } from 'url';
import { dirname, resolve } from 'path';
import dotenv from 'dotenv';

// Obteniendo el directorio actual del archivo actual
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Obteniendo la ruta absoluta del archivo .env
const envFilePath = resolve(__dirname, ".env");

// Cargando las variables de entorno desde el archivo .env
dotenv.config({ path: envFilePath });

// Exportando las variables de entorno
export const PORT = process.env.PORT;
export const HOST = process.env.HOST;
export const DB_URL = process.env.DB_URL;
export const ACCESS_JWT_SECRET = process.env.ACCESS_JWT_SECRET;
export const REFRESH_JWT_SECRET = process.env.REFRESH_JWT_SECRET;

export const FTP_URL = process.env.FTP_URL;
export const FTP_USER = process.env.FTP_USER;
export const FTP_PASSWORD = process.env.FTP_PASSWORD;
