import { handleError } from "../utils/errorHandler.js";
import Categoria from "../models/categoria.model.js";
import Publicacion from "../models/publicacion.model.js"

async function createPublicacion(publicacion) {
  try {
    const { nombre, detalle, categoria } = publicacion;

    // Busca las categorías en la base de datos
    const categoriasEncontradas = await Categoria.find({ name: { $in: categoria } });
    if (categoriasEncontradas.length === 0) return [null, "La categoría no existe..."];
    const categoriasIds = categoriasEncontradas.map((categoria) => categoria._id);

    const nuevaPublicacion = new Publicacion({
      nombre,
      detalle,
      categoria: categoriasIds, // Asigna los IDs de las categorías encontradas
      like: 0,
      dislike: 0,
      fecha: new Date() // Utiliza la fecha actual del sistema
    });
    await nuevaPublicacion.save();

    return [nuevaPublicacion, null];
  } catch (error) {
    handleError(error, "publicacion.service -> createUser");
    return [null, error.message]; // Retorna null y el mensaje de error en caso de fallo
  }
}

async function getPublicacion(){

  try {
    const  publicaciones = await Publicacion.find();
    if(!publicaciones) return [null, "No hay publicaciones"];
    return [publicaciones,null]
  } catch (error) {
    handleError(error, "publicacion.service -> getPublicacion");
    return [null, error.message]; // Retorna null y el mensaje de error en caso de fallo
  }
}


async function getPublicacionById(id) {
  try {
    const publicacion = await Publicacion.findById({ _id: id })

    if (!publicacion) return [null, "El Publicacion no existe"];

    return [publicacion, null];
  } catch (error) {
    handleError(error, "publicacion.service -> getPublicacionById");
  }
}

async function deletePublicacion(id) {
  try {
    return await Publicacion.findByIdAndDelete(id);
  } catch (error) {
    handleError(error, "publicacion.service -> deletepublicacionr");
  }
}


export default {
    createPublicacion,
    getPublicacion,
    getPublicacionById,
    deletePublicacion,
};
