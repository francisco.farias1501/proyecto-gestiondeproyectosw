"use strict";


import { handleError } from "../utils/errorHandler.js";
import Publicacion from "../models/publicacion.model.js"
import Reporte from "../models/reporte.model.js"

async function createReporte(reporte) {
    try {
        const { detalle, publicacion } = reporte;

        // Buscar la publicación por su ID
        const publicacionEncontrada = await Publicacion.findById(publicacion);
        if (!publicacionEncontrada) return [null, "La publicación no existe"];

        // Crear el nuevo reporte
        const nuevoReporte = new Reporte({
            detalle,
            publicacion: publicacionEncontrada._id // Asignar la ID de la publicación encontrada
        });
        await nuevoReporte.save();

        return [nuevoReporte, null];
    } catch (error) {
        handleError(error, "reporte.service.js -> createReporte");
        return [null, error.message];
    }
}

async function getReporte() {
    try {
        const reportes = await Reporte.find();
        if (!reportes || reportes.length === 0) return [null, "No se han encontrado reportes"];
        return [reportes, null];
    } catch (error) {
        handleError(error, "reporte.service.js -> getReporte");
        return [null, error.message];
    }
}

async function getReporteById(id) {
    try {
        const reporte = await Reporte.findById({ _id: id })
    
        if (!reporte) return [null, "El Reporte no existe"];
    
        return [reporte, null];
      } catch (error) {
        handleError(error, "reporte.service -> getReporteById");
      }
    }


async function deleteReporte(id) {
    try {
        const reporteEliminado = await Reporte.findByIdAndDelete(id);
        if (!reporteEliminado) return [null, "No se ha encontrado el reporte para eliminar"];
        return [reporteEliminado, null];
    } catch (error) {
        handleError(error, "reporte.service.js -> deleteReporte");
        return [null, error.message];
    }
}

export default {
    createReporte,
    getReporte,
    getReporteById,
    deleteReporte,
};
